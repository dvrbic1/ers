package com.revature.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.dao.ReimbersementUserDaoImp;
import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.service.UserService;

public class UserController {

	public static String confirm(HttpServletRequest req) {
		System.out.println("in user controller confirm");
		if(!req.getMethod().equals("POST")) {
			return "html/index.html";
		}
		
		UserService service1 = new UserService();
		User current = service1.getUserVerify(req.getParameter("username"), req.getParameter("password"));
		
		System.out.println(current == null);
		
		if (current == null) {
			
			return "/html/loginfailed.html";
			
		} else {
			if (current.getUser_role().equals("employee")) {
				req.getSession().setAttribute("currentUser", current);
				System.out.println("employee");
				return "html/welcomeemployee.html";

			} else {
				req.getSession().setAttribute("currentUser", current);
				System.out.println(current.getUser_role() == "employee");
				System.out.println("admin");
				return "html/welcomemanager.html";
			}
		}
}


	
	public static void getSessionUser(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		User current = (User)req.getSession().getAttribute("currentUser");
		System.out.println(current.toString());
		//res.setContentType("text/plain");
		res.getWriter().write(new ObjectMapper().writeValueAsString(current));
	//	res.getWriter().write(new ObjectMapper().writeValueAsString(vill));

	}
	
	public static void getSessionReimbursementList(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		User current = (User)req.getSession().getAttribute("currentUser");
		List<Reimbursement> reimblist = new ArrayList<Reimbursement>();
		reimblist = ReimbersementUserDaoImp.viewReimbursement(current, reimblist, req);
		res.getWriter().write(new ObjectMapper().writeValueAsString(reimblist));
		//System.out.println(current.toString());
		//res.setContentType("text/plain");
		//res.getWriter().write(new ObjectMapper().writeValueAsString(current));
	//	res.getWriter().write(new ObjectMapper().writeValueAsString(vill));

	}
	
}