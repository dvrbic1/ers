package com.revature.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.revature.model.User;
import com.revature.model.Reimbursement;


public class ReimbersementUserDaoImp {

private static final String URL = "jdbc:postgresql://rev-can-training.cdr933npo5dp.us-east-2.rds.amazonaws.com:5432/ticketdb";
	
	private static final String username = "ticketuser";
	private static final String password = "Passw0rd";
	
	public static void makereimb(HttpServletRequest req) {
		
		User newreimb = (User) req.getSession().getAttribute("currentUser");
		
		System.out.println(newreimb.toString());
		
		System.out.println(req.getParameter("value") + " Select Option");

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try(Connection con = DriverManager.getConnection(URL, username, password)) {
			
			String sql = "insert into user_reimbursement (reim_amount, reim_description, reimb_author, reimb_status, reimb_type, reim_submitted) values(?, ?, ?, ?, ?, ?)"; // ?? in string are placeholders
			//with our method arguements
			
			
			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setInt(1, Integer.parseInt(req.getParameter("reimamount"))); // these setString methods will replace the specified ? parameter in the sql with the getFirstName
			// value provided in the second argument 
			prepStatement.setString(2, req.getParameter("description"));
			prepStatement.setInt(3, newreimb.getUserID());
			prepStatement.setString(4, "Pending");
			prepStatement.setString(5, req.getParameter("retype"));
			prepStatement.setTimestamp(6, Timestamp.from(ZonedDateTime.now().toInstant()));
			System.out.println(Timestamp.from(ZonedDateTime.now().toInstant()));
			//System.out.println(req.getParameter("reimbtype") + " Select Option"); Timestamp.from(ZonedDateTime.now().toInstant())
			
			//System.out.println("num of rows changed " + changed);
			
			int changed = prepStatement.executeUpdate();
			System.out.println("dvknejkfjdojaodiwaj");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static List<Reimbursement> viewReimbursement(User current, List<Reimbursement> reimblist, HttpServletRequest req) {
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try(Connection con = DriverManager.getConnection(URL, username, password)) {
			if (current.getUser_role().equals("employee")) {
			//SELECT * FROM user_reimbursement WHERE reimb_author = 1;
			String sql = "SELECT * FROM user_reimbursement WHERE reimb_author = ?"; // ?? in string are placeholders
			//with our method arguements
			
			
			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setInt(1, current.getUserID()); // these setString methods will replace the specified ? parameter in the sql with the getFirstName
			// value provided in the second argument 
			
			//System.out.println(req.getParameter("reimbtype") + " Select Option"); Timestamp.from(ZonedDateTime.now().toInstant())
			
			//System.out.println("num of rows changed " + changed);
			
			//int changed = prepStatement.executeUpdate();
			
			ResultSet rs = prepStatement.executeQuery();
			
			while(rs.next()) {
				
				Reimbursement tester = new Reimbursement();
				tester.setAll(rs.getInt("reimb_id"), rs.getDouble("reim_amount"), rs.getTimestamp("reim_submitted"), rs.getTimestamp("reim_resolved"), rs.getString("reim_description"), rs.getInt("reimb_author"), rs.getInt("reimb_resolver"), rs.getString("reimb_status"), rs.getString("reimb_type"));
				reimblist.add(tester);
				
			}
			
			
			
			System.out.println("hellooooooooo");
			
			return reimblist;
			} else if (current.getUser_role().equals("admin")) {
				//SELECT * FROM user_reimbursement WHERE reimb_author = 1;
				String sql = "SELECT * FROM user_reimbursement"; // ?? in string are placeholders
				//with our method arguements
				
				
				PreparedStatement prepStatement = con.prepareStatement(sql);
				//prepStatement.setInt(1, current.getUserID()); // these setString methods will replace the specified ? parameter in the sql with the getFirstName
				// value provided in the second argument 
				
				//System.out.println(req.getParameter("reimbtype") + " Select Option"); Timestamp.from(ZonedDateTime.now().toInstant())
				
				//System.out.println("num of rows changed " + changed);
				
				//int changed = prepStatement.executeUpdate();
				
				ResultSet rs = prepStatement.executeQuery();
				
				while(rs.next()) {
					
					Reimbursement tester = new Reimbursement();
					tester.setAll(rs.getInt("reimb_id"), rs.getDouble("reim_amount"), rs.getTimestamp("reim_submitted"), rs.getTimestamp("reim_resolved"), rs.getString("reim_description"), rs.getInt("reimb_author"), rs.getInt("reimb_resolver"), rs.getString("reimb_status"), rs.getString("reimb_type"));
					reimblist.add(tester);
					System.out.println(tester.toString());
					
				}
				
				
				
				System.out.println("hellooooooooo");
				
				return reimblist;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return reimblist;
		
	}
	
public static void reaction(HttpServletRequest req) {
		
		User newreimb = (User) req.getSession().getAttribute("currentUser");
		
		System.out.println(newreimb.toString());
		
		System.out.println(req.getParameter("value") + " Select Option");

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try(Connection con = DriverManager.getConnection(URL, username, password)) {
			
			String sql = "UPDATE user_reimbursement SET reimb_status = ?, reim_resolved = ?, reimb_resolver = ? WHERE reimb_id = ?"; // ?? in string are placeholders
			//with our method arguements "UPDATE user_reimbursement SET reimb_status = ? WHERE reimb_id = ?";
			System.out.println(req.getParameter("reaction"));
			System.out.println(newreimb.getUserID());
			System.out.println(req.getParameter("reimid"));
			
			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setString(1, req.getParameter("reaction")); // these setString methods will replace the specified ? parameter in the sql with the getFirstName
			// value provided in the second argument 
			prepStatement.setTimestamp(2, Timestamp.from(ZonedDateTime.now().toInstant()));
			prepStatement.setInt(3, newreimb.getUserID());
			prepStatement.setInt(4, Integer.parseInt(req.getParameter("reimid")));
			
			//System.out.println(req.getParameter("reimbtype") + " Select Option"); Timestamp.from(ZonedDateTime.now().toInstant())
			
			//System.out.println("num of rows changed " + changed);
			
			int changed = prepStatement.executeUpdate();
			System.out.println("dvknejkfjdojaodiwaj");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
}
