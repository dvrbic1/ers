package com.revature.servlets;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.revature.controller.UserController;
import com.revature.dao.ReimbersementUserDaoImp;

public class RequestDispatcher {

public static String process(HttpServletRequest req) {
		
		switch(req.getRequestURI()) {
			case "/Project1/login.change":
				System.out.println("in Login.change dispatcher");
				return UserController.confirm(req);
			
			case "/Project1/logout.change":
				HttpSession session = req.getSession();
				session.invalidate();
				return "/html/login.html";
				
			case "/Project1/makereimb.change":
				System.out.println("in makereimb dispatcher");
				//System.out.println(req.getParameter("value") + " Select dispatcher");
				//System.out.println(req.getParameterNames());
//				Map params = req.getParameterMap();
//			    Iterator i = params.keySet().iterator();
//
//			    while ( i.hasNext() )
//			      {
//			        String key = (String) i.next();
//			        String value = ((String[]) params.get( key ))[ 0 ];
//			        System.out.println(key + " " + value);
//			      }
				ReimbersementUserDaoImp.makereimb(req);
				return "/html/makeReimbursement.html";
			
			case "/Project1/react.change":
				System.out.println("in reaction dispatcher");
				ReimbersementUserDaoImp.reaction(req);
				return "/html/approvedenyreimbursement.html";
				
			default:
					System.out.println("in default");
					return "/html/loginfailed.html";
				
			
		
		}
		
	}
	
}
