package com.revature.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.controller.UserController;

public class JSONDispatcherServlet {

public static void process(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
	System.out.println(req.getRequestURI());
	
		switch(req.getRequestURI()) {
		
		 case "/Project1/getsessionuser.json":
			 System.out.println("Inside JSON Dispatcher");
			 UserController.getSessionUser(req, res);
			 System.out.println("Inside JSON Dispatcher again");
			 
			 break;
			 
		 case "/Project1/getreimbursement.json":
			 System.out.println("Inside JSON reimbursement again");
			 UserController.getSessionReimbursementList(req, res);
			 System.out.println("Inside JSON reimbursementlist again");
			 break;
			 
		}
		
	}
}
