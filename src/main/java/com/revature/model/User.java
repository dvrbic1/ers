package com.revature.model;

import java.sql.Timestamp;
import java.util.Date;

public class User {

	private int userID;
	private String userName;
	private String userPass;
	private String firstName;
	private String lastName;
	private String email;
	private String user_role;
	
	public void setAll(int userID, String userName, String userPass, String firstName, String lastName, String email, String user_role) {
		
		this.userID = userID;
		this.userName = userName;
		this.userPass = userPass;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.user_role = user_role;
		
	}
	
	public User() {
		
	}
	
	public User(String userName, String userPass) {
		super();
		this.userPass = new String(userPass);
		this.userName = new String(userName);
//		System.out.println(this.userName);
//		System.out.println(this.userPass);
	}
	
	
	
	public User(int userID, String userName, String userPass, String firstName, String lastName, String email, String user_role) {
		super();
		this.userID = userID;
		this.userName = userName;
		this.userPass = userPass;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.user_role = user_role;
	}



	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUser_role() {
		return user_role;
	}
	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", userName=" + userName + ", userPass=" + userPass + ", firstName="
				+ firstName + ", lastName=" + lastName + ", email=" + email + ", user_role=" + user_role + "]";
	}
	
	
	
	
	
}
