package com.revature.model;

import java.sql.Timestamp;
import java.util.Date;

public class Reimbursement {

	private int reimbID;
	private double reimAmount;
	private Timestamp submitted;
	private Timestamp resolved;
	private String reimDesc;
	private int reimbAuthor;
	private int reimbResolver;
	private String reimStatus;
	private String reimbType;
	
	
	public Reimbursement() {
		
	}
	
	public Reimbursement(int reimbID, double reimAmount, Timestamp submitted, Timestamp resolved, String reimDesc,
			int reimbAuthor, int reimbResolver, String reimStatus, String reimbType) {
		super();
		this.reimbID = reimbID;
		this.reimAmount = reimAmount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.reimDesc = reimDesc;
		this.reimbAuthor = reimbAuthor;
		this.reimbResolver = reimbResolver;
		this.reimStatus = reimStatus;
		this.reimbType = reimbType;
	}




	public void setAll(int reimbID, double reimAmount, Timestamp submitted, Timestamp resolved, String reimDesc,
			int reimbAuthor, int reimbResolver, String reimStatus, String reimbType) {
		
		this.reimbID = reimbID;
		this.reimAmount = reimAmount;
		this.submitted = submitted;
		this.resolved = resolved;
		this.reimDesc = reimDesc;
		this.reimbAuthor = reimbAuthor;
		this.reimbResolver = reimbResolver;
		this.reimStatus = reimStatus;
		this.reimbType = reimbType;
		
	}





	public int getReimbID() {
		return reimbID;
	}










	public void setReimbID(int reimbID) {
		this.reimbID = reimbID;
	}










	public double getReimAmount() {
		return reimAmount;
	}










	public void setReimAmount(double reimAmount) {
		this.reimAmount = reimAmount;
	}










	public Timestamp getSubmitted() {
		return submitted;
	}










	public void setSubmitted(Timestamp submitted) {
		this.submitted = submitted;
	}










	public Timestamp getResolved() {
		return resolved;
	}










	public void setResolved(Timestamp resolved) {
		this.resolved = resolved;
	}










	public String getReimDesc() {
		return reimDesc;
	}










	public void setReimDesc(String reimDesc) {
		this.reimDesc = reimDesc;
	}










	public int getReimbAuthor() {
		return reimbAuthor;
	}










	public void setReimbAuthor(int reimbAuthor) {
		this.reimbAuthor = reimbAuthor;
	}










	public int getReimbResolver() {
		return reimbResolver;
	}










	public void setReimbResolver(int reimbResolver) {
		this.reimbResolver = reimbResolver;
	}










	public String getReimStatus() {
		return reimStatus;
	}










	public void setReimStatus(String reimStatus) {
		this.reimStatus = reimStatus;
	}










	public String getReimbType() {
		return reimbType;
	}










	public void setReimbType(String reimbType) {
		this.reimbType = reimbType;
	}










	@Override
	public String toString() {
		return "Reimbursement [reimbID=" + reimbID + ", reimAmount=" + reimAmount + ", submitted=" + submitted
				+ ", resolved=" + resolved + ", reimDesc=" + reimDesc + ", reimbAuthor=" + reimbAuthor
				+ ", reimbResolver=" + reimbResolver + ", reimStatus=" + reimStatus + ", reimbType=" + reimbType + "]";
	}
	
	
	
	
	
	
	
}
