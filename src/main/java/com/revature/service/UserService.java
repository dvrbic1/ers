package com.revature.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.revature.model.User;

public class UserService {
	
	private static final String URL = "jdbc:postgresql://rev-can-training.cdr933npo5dp.us-east-2.rds.amazonaws.com:5432/ticketdb";
	
	private static final String username = "ticketuser";
	private static final String password = "Passw0rd";
	
	public User getUserVerify(String uName, String uPass) {
		System.out.println("Inside User verify");
		
		User placeholder = new User();
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try(Connection con = DriverManager.getConnection(URL, username, password)) {
			
			String sql = "SELECT * FROM user_table WHERE user_name = ? AND user_pass = ?"; // ?? in string are placeholders
			//with our method arguements
			
			
			PreparedStatement prepStatement = con.prepareStatement(sql);
			prepStatement.setString(1, uName); // these setString methods will replace the specified ? parameter in the sql with the
			// value provided in the second argument
			prepStatement.setString(2, uPass);
			
			//System.out.println("num of rows changed " + changed);
			
			ResultSet rs = prepStatement.executeQuery();
			if (rs.next()) {
				
				//int userID, String userName, String userPass, String firstName, String lastName, String email, String user_role
				placeholder.setAll(rs.getInt("user_id"), rs.getString("user_name"), rs.getString("user_pass"), rs.getString("user_firstname"), rs.getString("user_lastname"), rs.getString("user_email"), rs.getString("user_role"));
				return placeholder;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
//		boolean check1;
//		boolean check2;
//		boolean check3;
//		check1 = placeholder.getUserName().equals(uName);
//		check2 = placeholder.getUserPass().equals(uPass);
//		check3 = check1 && check2;
//		
//		if (check3) {
//			System.out.println("Returning false");
//
//			return placeholder;
//		} else {
//			System.out.println("Returning true");
//
//			return null;
//		}
		
		return null;
		
	}
	
}
